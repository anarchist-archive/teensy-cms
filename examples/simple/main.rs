use actix_web::middleware::Logger;
use actix_web::{get, web, App, HttpResponse, HttpServer};
use serde::Serialize;
use std::path::PathBuf;
use teensy_cms::{DefaultPage, Menu, TeensyCms};
use tera::{Context, Tera};

lazy_static::lazy_static! {
    // helper for this example
    static ref DIR: PathBuf = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
        .join("examples")
        .join("simple");

    // the CMS for admin controlled resources
    static ref CMS: TeensyCms<DefaultPage> = {
        TeensyCms::from_config_path("/page", DIR.join("cms").join("teensy.yml")).unwrap()
    };

    // the template engine for the application's own templates
    static ref TEMPLATES: Tera = {
        let mut tera = Tera::new(&format!("{}/*.html", DIR.join("site").as_os_str().to_str().unwrap())).unwrap();
        tera.autoescape_on(vec![".html"]);
        tera
    };
}

#[derive(Serialize)]
struct SimplePage<'a> {
    nav: &'a Menu,
    page_inner: &'a str,
}

// this route is configured by the application developer and is where an admin's resources will be
// hosted
#[get("/page/{tail:.*}")]
async fn page(cms_page: web::Path<String>) -> HttpResponse {
    let page_inner = CMS
        .render(&(cms_page.as_ref().to_owned() + ".html"))
        .unwrap();
    let body = TEMPLATES
        .render(
            "simple-page.html",
            &Context::from_serialize(SimplePage {
                nav: CMS.menu(),
                page_inner: &page_inner,
            })
            .unwrap(),
        )
        .unwrap();
    HttpResponse::Ok()
        .insert_header(("Content-Type", "text/html"))
        .body(body)
}

#[derive(Serialize)]
struct IndexPage<'a> {
    nav: &'a Menu,
}

// this index page is controlled by the application developer
async fn index() -> HttpResponse {
    let body = TEMPLATES
        .render(
            "index.html",
            &Context::from_serialize(IndexPage { nav: CMS.menu() }).unwrap(),
        )
        .unwrap();
    HttpResponse::Ok()
        .insert_header(("Content-Type", "text/html"))
        .body(body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    println!("\n\n  Running server on 127.0.0.1:8080\n");
    HttpServer::new(|| {
        App::new()
            .wrap(Logger::new("\"%r\" %s"))
            .route("/", web::get().to(index))
            .service(page)
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
