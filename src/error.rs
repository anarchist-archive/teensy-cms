//! Error types
use std::path::PathBuf;

/// Wrapper type of [`std::result::Result`] with `Err` as [`Error`].
pub type Result<T> = std::result::Result<T, Error>;

/// Error type
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    /// When loading pages, there was a collision for items in a menu.
    #[error("Duplicate menu item: {}", .0)]
    DuplicateMenuItem(String),

    /// When loading pages, there was a collision for URLs.
    #[error("Duplicate URL: {}", .0)]
    DuplicateUrl(String),

    /// A path was not able to be processed by the application.
    #[error("Path")]
    InvalidPath(PathBuf),

    /// IO error
    #[error("I/O error")]
    Io(#[from] std::io::Error),

    /// The frontmatter for a page was malformed
    #[error("Malformed frontmatter")]
    MalformedFrontmatter,

    /// The frontmatter for a page was not found
    #[error("No frontmatter")]
    NoFrontmatter,

    /// When attempting to render a page, it was not found
    #[error("Page not found")]
    PageNotFound,

    /// There was an error rendering the page
    #[error("Tera error")]
    Tera(#[from] tera::Error),

    /// There was an parsing the frontmatter
    #[error("YAML de/serialization error")]
    Yaml(#[from] serde_yaml::Error),
}
