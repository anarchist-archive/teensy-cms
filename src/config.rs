//! Configuration for the CMS
use serde::{Deserialize, Serialize};
use std::path::PathBuf;

/// Configuration for the application.
///
/// When it is loaded by [`crate::TeensyCms::from_config_path`], all paths are considered related
/// to the config file itself.
///
/// In YAML format, the config would look like:
///
/// ```yaml
/// ---
/// pages:
///   - path: about.html
///     url: /about
///   - title: Submenu
///     pages:
///       - path: sub/page.html
///         url:  sub/page
/// template_extras:
///   # directories
///   - macros/
///   # single files
///   - layout.html
///   # globs
///   - helper/*.html
/// ```
#[derive(Deserialize, Debug)]
pub struct Config {
    /// A list of pages and submenus to include.
    pub pages: Vec<MenuItemConfig>,
    /// Extra templates to load into the rendering environment.
    pub template_extras: Option<Vec<String>>,
}

/// Configurations for menu items.
#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum MenuItemConfig {
    /// A singple page in the menu.
    Page {
        /// The path on the file system to the template file.
        path: PathBuf,
        /// A URL for this page. This will still be relative to the page base and not absolute across
        /// the application.
        url: String,
        /// If and where the page will be visible.
        #[serde(default = "Visibility::default")]
        visibility: Visibility,
    },
    /// A grouping of menu items.
    Menu {
        /// Title of the menu group.
        title: String,
        /// A list of pages under the menu group.
        pages: Vec<MenuItemConfig>,
    },
}

/// Enum for a page's visiblity in menus.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
#[serde(rename_all = "kebab-case")]
pub enum Visibility {
    /// Page is always visible in all menus.
    Always,
    /// Page is never visible in any menus.
    Never,
    /// Page is only visible in header menus.
    HeaderOnly,
    /// Page is only visible in footer menus.
    FooterOnly,
}

impl Visibility {
    pub fn show_in_header(&self) -> bool {
        match self {
            Self::Always => true,
            Self::Never => false,
            Self::HeaderOnly => true,
            Self::FooterOnly => false,
        }
    }

    pub fn show_in_footer(&self) -> bool {
        match self {
            Self::Always => true,
            Self::Never => false,
            Self::HeaderOnly => false,
            Self::FooterOnly => true,
        }
    }
}

impl Default for Visibility {
    fn default() -> Self {
        Self::Always
    }
}
