//! TeensyCMS is the smallest CMS possible that allows admins running your application to add custom
//! pages that are accessible from a nav bar.
//!
//! An implementation of TeensyCMS with [`actix-web`](https://actix.rs/) can be found in the source
//! code's example directory.
//!
//! ```no_run
//! # struct MyConfig {
//! #     pages_config_path: &'static str,
//! # }
//! # impl MyConfig {
//! #     fn from_env() -> Self { todo!() }
//! # }
//! # struct Request {}
//! # impl Request {
//! #     fn data<T>(&self) -> Option<T> { todo!() }
//! #     fn path_args(&self) -> std::collections::HashMap<&'static str, String> { todo!() }
//! # }
//! use teensy_cms::{TeensyCms, DefaultPage};
//!
//! let my_config = MyConfig::from_env();
//! let cms = TeensyCms::<DefaultPage>::from_config_path(
//!     "/page",
//!     &my_config.pages_config_path
//! ).unwrap();
//!
//! /* -- server initialization here -- */
//!
//! fn handle_page_request(req: Request) -> String {
//!     // something like "contact" or "about"
//!     let page = &req.path_args()["page"];
//!     req.data::<TeensyCms<DefaultPage>>().unwrap()
//!         .render(&format!("{page}.html")).unwrap()
//! }
//! ```
//!
//! Pages use YAML frontmatter followed by HTML.
//! The frontmatter **MUST** be delimited by `---` both before and after.
//! Only a single YAML document is permitted in the frontmatter.
//!
//! ```yaml
//! ---
//! title: My Page
//! cats: [Scruffles, Mx. Clawz]
//! ---
//! <h1>{{ page.title }}</h1>
//! <ul>
//!   {% for cat in page.cats %}
//!     <li>{{ cat }}</li>
//!   {% endfor %}
//! </ul>
//! ```
use serde::Serialize;
use std::collections::{HashMap, HashSet};
use std::fs::{self, File};
use std::path::{Path, PathBuf};
use tera::{Context, Tera};
use walkdir::WalkDir;

mod config;
mod error;
mod menu;
mod page;

pub use config::{Config, MenuItemConfig, Visibility};
pub use error::{Error, Result};
pub use menu::{Menu, MenuItem};
pub use page::{DefaultPage, Page};

#[cfg(windows)]
const FRONT_MATTER_DELIMITER: &str = "---\r\n";
#[cfg(not(windows))]
const FRONT_MATTER_DELIMITER: &str = "---\n";

/// The main CMS struct.
#[derive(Debug)]
pub struct TeensyCms<P: Page> {
    pages: HashMap<String, P>,
    tera: Tera,
    menu: Menu,
}

impl<P: Page> TeensyCms<P> {
    /// Loads the CMS from a directory and strips the directory's prefix from the template names.
    pub fn from_config_path(
        url_root: impl AsRef<str>,
        config_path: impl AsRef<Path>,
    ) -> Result<TeensyCms<P>> {
        let config_path = config_path.as_ref().canonicalize()?;

        #[cfg(feature = "logging")]
        log::debug!(
            "Initializing TeensyCMS using config at: {}",
            config_path.display()
        );

        let config: Config = serde_yaml::from_reader(File::open(&config_path)?)?;
        let root_path_components = config_path.components().count() - 1;
        let root_path = config_path.parent().unwrap();

        let mut tera = Tera::default();
        // we need to load possible parent templates before loading the child templates
        if let Some(extras) = &config.template_extras {
            for extra in extras {
                let extra = root_path.join(extra);
                match extra.to_str().and_then(|p| glob::glob(p).ok()) {
                    Some(path_iter) => {
                        for path in path_iter.filter_map(|e| e.ok()) {
                            add_tera_template(root_path_components, &path, &mut tera)?;
                        }
                    }
                    None => {
                        for entry in WalkDir::new(extra)
                            .into_iter()
                            .filter_entry(|e| e.file_type().is_file())
                            .filter_map(|e| e.ok())
                        {
                            add_tera_template(root_path_components, entry.path(), &mut tera)?;
                        }
                    }
                }
            }
        } else {
            #[cfg(feature = "logging")]
            log::debug!("No template extras configured, not loading any.");
        }

        let mut pages = HashMap::new();
        let mut seen_urls = HashSet::new();
        let menu = Self::register_pages(
            root_path,
            root_path_components,
            url_root.as_ref(),
            &config.pages,
            &mut tera,
            &mut pages,
            &mut seen_urls,
        )?;

        #[cfg(feature = "logging")]
        log::debug!("CMS successfully initialized.");
        Ok(Self { pages, tera, menu })
    }

    fn register_pages(
        root_path: &Path,
        root_path_components: usize,
        root_url: &str,
        menu_item_configs: &[MenuItemConfig],
        tera: &mut Tera,
        pages: &mut HashMap<String, P>,
        seen_urls: &mut HashSet<String>,
    ) -> Result<Menu> {
        let mut menu = Menu(Vec::new());

        for menu_item_config in menu_item_configs {
            match menu_item_config {
                MenuItemConfig::Page {
                    path,
                    url,
                    visibility,
                } => {
                    #[cfg(feature = "logging")]
                    log::debug!("Parsing page with relative page: {:?}", path);

                    if seen_urls.contains(url) {
                        return Err(Error::DuplicateUrl(url.to_string()));
                    }
                    seen_urls.insert(url.clone());

                    let full_path = root_path.join(path);
                    let template_str = fs::read_to_string(&full_path)?;
                    let (page, content) = parse_page::<P>(&template_str)?;

                    let page_title = page.title();
                    for current_item in menu.iter() {
                        if current_item.title() == page_title {
                            return Err(Error::DuplicateMenuItem(page_title));
                        }
                    }

                    let template_path = full_path
                        .components()
                        .skip(root_path_components)
                        .collect::<PathBuf>();
                    let template_path = template_path
                        .to_str()
                        .ok_or_else(|| Error::InvalidPath(template_path.clone()))?;

                    #[cfg(feature = "logging")]
                    log::debug!(
                        "Adding template {:?} for page {:?}",
                        template_path,
                        page_title
                    );

                    menu.0.push(MenuItem::Page {
                        title: page_title,
                        url: url_join(root_url, url),
                        visibility: *visibility,
                    });
                    pages.insert(template_path.to_string(), page);
                    tera.add_raw_template(template_path, content)?;
                }
                MenuItemConfig::Menu {
                    title,
                    pages: menu_item_configs,
                } => {
                    for current_item in menu.iter() {
                        if current_item.title() == title {
                            return Err(Error::DuplicateMenuItem(title.to_string()));
                        }
                    }

                    let submenu = Self::register_pages(
                        root_path,
                        root_path_components,
                        root_url,
                        menu_item_configs,
                        tera,
                        pages,
                        seen_urls,
                    )?;

                    menu.0.push(MenuItem::Menu {
                        title: title.to_string(),
                        menu: submenu,
                    });
                }
            };
        }

        Ok(menu)
    }

    /// Render a page by name.
    pub fn render(&self, page: &str) -> Result<String> {
        match self.pages.get(page) {
            Some(page_data) => Ok(self.tera.render(page, &get_context(page_data)?)?),
            None => Err(Error::PageNotFound),
        }
    }

    /// Return the frontmatter for a page by name.
    pub fn frontmatter(&self, page: &str) -> Option<&P> {
        self.pages.get(page)
    }

    /// Get a reference to the CMS's generated [`Menu`].
    pub fn menu(&self) -> &Menu {
        &self.menu
    }
}

#[derive(Serialize)]
struct ConextHelper<'a, P: Page> {
    page: &'a P,
}

#[inline]
fn get_context<P: Page>(page: &P) -> Result<Context> {
    Ok(Context::from_serialize(ConextHelper { page })?)
}

fn parse_page<P: Page>(page_str: &str) -> Result<(P, &str)> {
    if !page_str.starts_with(FRONT_MATTER_DELIMITER) {
        return Err(Error::NoFrontmatter);
    }
    let end_idx = match &page_str[FRONT_MATTER_DELIMITER.len()..].find(FRONT_MATTER_DELIMITER) {
        Some(idx) => *idx + FRONT_MATTER_DELIMITER.len(),
        None => return Err(Error::MalformedFrontmatter),
    };
    let yaml_str = &page_str[0..end_idx];
    let page = serde_yaml::from_str(yaml_str)?;
    let content = &page_str[(end_idx + FRONT_MATTER_DELIMITER.len())..];
    Ok((page, content))
}

fn url_join(root: &str, rest: &str) -> String {
    let root = root.trim_end_matches('/');
    let rest = rest.trim_start_matches('/');
    let mut joined = String::with_capacity(root.len() + rest.len() + 1);
    joined.push_str(root);
    joined.push('/');
    joined.push_str(rest);
    joined
}

fn add_tera_template(root_path_components: usize, path: &Path, tera: &mut Tera) -> Result<()> {
    let template_path = path
        .components()
        .skip(root_path_components)
        .collect::<PathBuf>();
    let template_path = template_path
        .to_str()
        .ok_or_else(|| Error::InvalidPath(template_path.clone()))?;
    let template = fs::read_to_string(path)?;

    #[cfg(feature = "logging")]
    log::debug!(
        "Loading template extra {:?} from path {:?}",
        template_path,
        path.display()
    );
    tera.add_raw_template(template_path, &template)?;
    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    fn get_cms(test_name: &str) -> Result<TeensyCms<DefaultPage>> {
        let config_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("dev-data")
            .join(test_name)
            .join("teensy.yml");
        TeensyCms::<DefaultPage>::from_config_path("/page", config_path)
    }

    #[test]
    fn parse_success() {
        let template = format!(
            "{}title: foo\n{}my page data",
            FRONT_MATTER_DELIMITER, FRONT_MATTER_DELIMITER
        );
        let (page, content) = parse_page::<DefaultPage>(&template).unwrap();
        let expected_page = DefaultPage {
            title: "foo".to_string(),
            extra: serde_yaml::Mapping::new(),
        };
        assert_eq!(page, expected_page);
        assert_eq!(content, "my page data");
    }

    #[test]
    fn simple() {
        let cms = get_cms("simple").unwrap();
        let page = cms.render("about.html").unwrap();
        assert_eq!(
            page.trim(),
            "<p>This is an about page with no template usage.</p>"
        );
    }

    #[test]
    fn missing_dir() {
        let err = get_cms("REALLY-DEFINITELY-DOES-NOT-EXIST").unwrap_err();
        assert!(matches!(err, Error::Io(_)));
    }

    #[test]
    fn err_menu_collisions() {
        let err = get_cms("duplicate-menu-error").unwrap_err();
        assert!(matches!(err, Error::DuplicateMenuItem(x) if x == "About"));
    }

    #[test]
    fn err_url_collisions() {
        let err = get_cms("duplicate-url-error").unwrap_err();
        assert!(matches!(err, Error::DuplicateUrl(x) if x == "/about"));
    }

    #[test]
    fn nested_menu() {
        let cms = get_cms("nested-menu").unwrap();
        let page = cms.render("cats/beans.html").unwrap();
        assert_eq!(page.trim(), "<p>This page is about the cat Beans.</p>");

        let expected_menu = Menu(vec![
            MenuItem::Page {
                title: "About".to_string(),
                url: "/page/about".to_string(),
                visibility: Default::default(),
            },
            MenuItem::Menu {
                title: "Cats".to_string(),
                menu: Menu(vec![
                    MenuItem::Page {
                        title: "Beans".to_string(),
                        url: "/page/cats/beans".to_string(),
                        visibility: Default::default(),
                    },
                    MenuItem::Page {
                        title: "Miette".to_string(),
                        url: "/page/cats/miette".to_string(),
                        visibility: Default::default(),
                    },
                ]),
            },
        ]);

        assert_eq!(cms.menu, expected_menu);
    }

    #[test]
    fn template_extras() {
        let cms = get_cms("template-extras").unwrap();
        let page = cms.render("test.html").unwrap();
        assert_eq!(page.trim(), "<p>Layout: Test</p>");
    }
}
