//! Menu and nav utilities.
use crate::config::Visibility;
use serde::Serialize;

/// Contains the nested menu items after loading all pages.
///
/// When rendered to JSON, it will be an array with items in one of these two formats:
///
/// ## Item
///
/// ```json
/// {
///     "type": "page",
///     "title": "Some Title",
///     "url": "/full/path/some-title",
///     "visibility": "always"
/// }
/// ```
/// ## Menu
///
/// ```json
/// {
///     "type": "menu",
///     "title": "Some Menu",
///     "menu": [ ... ]
/// }
/// ```
#[derive(Serialize, Debug, PartialEq, Eq, Clone)]
pub struct Menu(pub(crate) Vec<MenuItem>);

impl Menu {
    /// An iterator over the [`MenuItem`]s.
    pub fn iter(&self) -> impl Iterator<Item = &MenuItem> {
        self.0.iter()
    }
}

/// A menu item.
///
/// For JSON representation, see [`Menu`].
#[derive(Serialize, Debug, PartialEq, Eq, Clone)]
#[serde(tag = "type", rename_all = "lowercase")]
pub enum MenuItem {
    /// A single, clickable menu item.
    Page {
        /// The menu item's title.
        title: String,
        /// The full URL.
        url: String,
        /// The page's visibility. See [`crate::config::Visibility`] for more information.
        visibility: Visibility,
    },
    /// A menu with a title and nested items.
    Menu {
        /// The menu item's title.
        title: String,
        /// The nested submenu.
        menu: Menu,
    },
}

impl MenuItem {
    /// Helper function to access the title of the two variants.
    pub fn title(&self) -> &str {
        match self {
            Self::Page { title, .. } => title,
            Self::Menu { title, .. } => title,
        }
    }
}
