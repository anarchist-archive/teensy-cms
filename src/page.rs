//! Frontmatter de/serialization helpers.
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};

/// Trait for a page's frontmatter that loaded from a template.
pub trait Page: Serialize + DeserializeOwned {
    /// The page's title it should be displayed in a nav bar or menu.
    fn title(&self) -> String;
}

/// A struct that collects all values and makes them available to the page or templates.
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone)]
pub struct DefaultPage {
    pub title: String,
    /// All other fields in the frontmatter.
    #[serde(flatten, skip_serializing_if = "serde_yaml::Mapping::is_empty")]
    pub extra: serde_yaml::Mapping,
}

impl Page for DefaultPage {
    fn title(&self) -> String {
        self.title.clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use serde_yaml::Value;

    // ensures api stability
    #[test]
    fn default_page_de_serializes_missing_extra() {
        let yaml: Value = serde_yaml::from_str("title: Title").unwrap();
        let parsed: DefaultPage = serde_yaml::from_value(yaml.clone()).unwrap();
        assert!(parsed.extra.is_empty());
        let written = serde_yaml::to_value(&parsed).unwrap();
        assert_eq!(written, yaml);
    }

    // ensures api stability
    #[test]
    fn default_page_de_serializes_extra() {
        let yaml: Value = serde_yaml::from_str("title: Title\nmoar: stuff").unwrap();
        let parsed: DefaultPage = serde_yaml::from_value(yaml.clone()).unwrap();
        assert_eq!(&parsed.extra["moar"], "stuff");
        let written = serde_yaml::to_value(&parsed).unwrap();
        assert_eq!(written, yaml);
    }
}
