# TeensyCMS

A very small, very minimal CMS for allowing admins deploying your web applications to add custom pages and have them accessible from a nav bar.

A typical application using TeensyCMS dos so like this:

- Check for a `pages_dir` variable in its config
- Load the TeensyCMS from that path on the file system
- Have nav bars in the site's template use the values returned from TeensyCMS
- Mount a single route like `/pages/{tail:.*}` that renders TeensyCMS content

That's it.

A typical use case for this would be if you wanted a admin to be able to provide their own pages that may or maybe not include:

- An `About` page
- A `Contact` page
- A `Terms of Service` page
- A `Code of Conduct` page
- Whatever else they want idk a list of their favorite cats

A full working example of a website implementing TeensyCMS can be found in the [`examples/`](./examples) directory and run with `cargo run`, but for reference a simple app might look like this:

```rust
use teensy_cms::{TeensyCms, DefaultPage};

let my_config = MyConfig::from_env();
let cms = TeensyCms::<DefaultPage>::from_config_path(&my_config.pages_config_path).unwrap();
// finish initializing the web server

// imagine some fancy routing macro that wraps this
fn handle_page_request(req: Request) -> String {
    // something like "contact" or "about"
    let page = &req.path_args()["page"];
    req.data::<TeensyCms<DefaultPage>>().unwrap()
        .render(&format!("{page}.html")).unwrap()
}
```

Templates are loaded relative to where the config was loaded from.
Configs look like this:

```yaml
---
pages:
  - path: about.html
    url: /about
  - title: Submenu
    pages:
      - path: sub/page.html
        url:  sub/page
```

A page looks like the following:

```yaml
---
title: My Page
cats: [Scruffles, Mx. Clawz]
---
<h1>{{ page.title }}</h1>
<ul>
  {% for cat in page.cats %}
    <li>{{ cat }}</li>
  {% endfor %}
</ul>
```

## Anti-Copyright

Intellectual property isn't real.
There is no license.
If you insist on having one, this is [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/) (public domain).
